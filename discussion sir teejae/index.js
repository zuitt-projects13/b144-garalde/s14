/*Writing Comment in JavaScript*/
// for one line comment ctrl + /
/* multi-line comment ctrl + shift + / */

console.log("Hello Batch 144");

console.log("Hazel Garalde");


// Mini Activity

// console.log("Sisig");
// console.log("Sisig");
// console.log("Sisig");

let	food1 = "Sisig";

console.log("My favorite food is: " + food1)

let food2 = "Kare-kare";

console.log("My favorite foods are " + food1 + " and " + food2);

let	food3;

console.log(food3);	

food3 = "Chicken Joy";

console.log(food3);

// Mini Activity

food1 = "Kaldereta";
food2 = "Sinigang"

console.log(food1);
console.log(food2);

// let food1 = "Flat Tops";

/*
	const keyword - create CONSTANT variable. means that the data cannot be updated later 

*/

const pi = 3.1416;
console.log(pi);

// trying to update a const variable will result in an error

/*
const pi = "pizza";
console.log(pi);*/


// we also cannot declare a const variable without an initial value

/*const gravity;

console.log(gravity);*/

// mini-activity

let myName;



const sunriseDirection = "East";
const sunsetDirection = "West";

console.log(myName,sunriseDirection,sunsetDirection);


/*
	Guide in creating JS variable:

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declareation of the variable name and Initializetion of the initial value of the variable usng an assignment operator (=)

	3. A let variable can be declared without inititalization. However the value of the variable will be undefined until it is re-assigned with a value.

	4. Not Defined vs Undefined. Not Defined error means the variable is used but not declared. Undefined results from a variable that is used but is not initialized.

	5. We can use const keyword to create constant variables. Constant variables cannot be declared without initializetion. Constant variables cannot be reassigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add a space for variables with words as names.


*/

console.log("sample string data");
let country = "Philippines";
let province = "Rizal";
console.log(province,country);

let fullAddress = province + ' , ' + country;

console.log(fullAddress);


let greeting = "I live in " + country;
console.log(greeting);


let numstring = "50";
let numstring2 = "25";

console.log(numstring + numstring2);
console.log(numstring , numstring2);


let hero = "Captain America";
console.log(hero.length);


let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

let sum1 = num1 + num2;

console.log(sum1);

let numstring3 = "100";
let sum2 = parseInt(numstring3) + num1;
console.log(sum2)


// mini activity

let sum3 = sum1 + sum2;
let sum4 = parseInt(numstring2) + num2;
console.log(sum3+sum4);


let difference = num1-num2;


console.log(difference);


let difference2=numstring3-num2;
console.log(difference2);

let difference3 = hero-num2;
console.log(difference3);

// mutiplication

let num3=10;
let num4=5;

let product=num3*num4;
console.log(product);

let product2=numstring3*num3;


console.log(product2);


let product3=numstring3*numstring;
console.log(product3);


// division

let num5=30;
let num6=3;
let quotient=num5/num6
console.log(quotient);

let quotient2=numstring3/5;
console.log(quotient2);

let quotient3=450/num4;
console.log(quotient3);


let isAdmin=true;
// this is a boolean can be answered with yes or no. true false

// array separated with a comma
// make sure that data type of all items in the array are the same

let koponanNiEugene = ["Eugene","Alfred","Dennis","Vincent"]
console.log(koponanNiEugene);

// array index. starts from 0. markes the order of the items in the array
console.log(koponanNiEugene[0]);

let array2 = ["One Punch Man",true,500,"Saitama"];
console.log(array2);

/*
objects
data type used to mimic real world objects

each value is given a label which makes the data significant and meaningful. this pairing if data and "labal" is called a Key-Value Pair

a key-value pair together is called a property

each property is separated by a comma 
*/


let	person1 = {

	heroName: "One Punch Man",
	isRegistered:true,
	balance:500,
	realName:"Saitama"
};

console.log(person1.realName);



let myFavoriteBands = ["One OK Rock","Parokya ni Edgar","EraserHeads"];

let me = {
		firstName: "Hazel",
		lastName: "Garalde",
		isWebDeveloper: true,
		hasPortfolio: true,
		age: 26
};


let sampleNull=null;

let foundResult=null;

// for undefined the variable exist but the value is still unknown




	

